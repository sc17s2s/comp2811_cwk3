#pragma once
#include <QtWidgets>
#include <QtGui>
#include <QCheckBox>
#include <QObject>
#include "gitpp.h"

class QLabel;
class QCheckBox;
class QLineEdit;
class QWidget;
class QPushButton;

class configureTab: public QWidget{
  Q_OBJECT
public:
  configureTab();
  std::string path = ".";

private:
  bool fullConfigFile = true;
  void createWidgets();
  void arrangeWidgets();
  void setWidgetsDisabled();
  void getConfigValues();

  QMessageBox* msgBox = new QMessageBox();
  //QLineEdit* fieldName;
  QLabel* firstLine = new QLabel("Your .config file details:");
  QLabel* pathLine = new QLabel("Path of .git repository: \"" +
                QString::fromStdString(path) + "\"" );

////////////CONFIG VARIABLES////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
  QLabel* nameLabel = new QLabel("user.name");
  QLineEdit* nameValue = new QLineEdit(" ");
  QPushButton* editButton = new QPushButton("Edit");

  QLabel* emailLabel = new QLabel("user.email");
  QLineEdit* emailValue = new QLineEdit(" ");
  QPushButton* editButton2 = new QPushButton("Edit");

  QLabel* coreBareLabel = new QLabel("core.bare");
  QCheckBox* checkbox = new QCheckBox("true");

  QLabel* logallRefUpdatesLabel = new QLabel("logalrefupdates");
  QCheckBox* checkbox2 = new QCheckBox("true");

  //not in empty
  QLabel* branchMasterLabel = new QLabel("branchMasterLabel");
  QLineEdit* branchMasterValue = new QLineEdit(" ");
  QPushButton* editButton3 = new QPushButton("Edit");

  QLabel* reposFormatVersionLabel = new QLabel("repositoryformatversion");
  QLineEdit* reposFormatVersionValue = new QLineEdit(" ");
  QPushButton* editButton4 = new QPushButton("Edit");

  //not in empty
  QLabel* branchMasterMergeLabel = new QLabel("branchMasterMergeLabel");
  QLineEdit* branchMasterMergeValue = new QLineEdit(" ");
  QPushButton* editButton5 = new QPushButton("Edit");

  //not in empty
  QLabel* remoteOriginFetchLabel = new QLabel("fetch");
  QLineEdit* remoteOriginFetchValue = new QLineEdit(" ");
  QPushButton* editButton6 = new QPushButton("Edit");

  QLabel* coreFileLabel = new QLabel("filemode");
  QCheckBox* checkbox3 = new QCheckBox("true");

  QLabel* remoteOriginURLLabel = new QLabel("url");
  QLineEdit* remoteOriginURLValue = new QLineEdit(" ");
  QPushButton* editButton7 = new QPushButton("Edit");

  QPushButton* apply = new QPushButton("Apply");
  QPushButton* cancel = new QPushButton("Cancel");

  QPushButton* editButtonCb = new QPushButton("Edit");
  QPushButton* editButtonCb2 = new QPushButton("Edit");
  QPushButton* editButtonCb3 = new QPushButton("Edit");

public slots:
void enableField(QWidget* fieldName);
void cancelChanges();
void applyChanges();
// void enableField(QLineEdit* fieldName);
// void enableField(QLineEdit* fieldName);
// void enableField(QLineEdit* fieldName);
// void enableField(QLineEdit* fieldName);
// void enableField(QLineEdit* fieldName);
// void enableField(QLineEdit* fieldName);
};
