#include <QtWidgets>
#include "gitpp7.h"

class MyWindow : public QWidget
{
Q_OBJECT

public:
  MyWindow();
  void arrangeWidgets();

public slots:
  void selectRepo();

public:
  void makeConnection();

private:
  QLabel *selectLabel = new QLabel("Select directory to make a repo");
  QLabel *repoLabel = new QLabel("<h1>Select a repo</h1>");
  QPushButton *selectButton = new QPushButton("Create repo");
  QComboBox *selectDir = new QComboBox();
  QString selected;
};
