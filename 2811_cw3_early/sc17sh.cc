#include "sc17sh.h"

MyWindow::MyWindow() : QWidget()
{
  this->arrangeWidgets();
  this->makeConnection();
  this->setWindowTitle("My Window");
  this->setMinimumSize(640, 400);
}

void MyWindow::arrangeWidgets()
{

  searchLabel = new QLabel("Search your commits:");
  searchBar = new QLineEdit();
  commitTable = new QTableWidget(0, 4);
  searchButton = new QPushButton("Search");

  commitTable->setHorizontalHeaderLabels(QStringList() << "Commit Message"
    << "Author" << "Date" << "Commit ID");
  commitTable->verticalHeader()->hide();

  commitTable->setEditTriggers(QAbstractItemView::NoEditTriggers);

  QHeaderView* header = commitTable->horizontalHeader();
  header->setSectionResizeMode(QHeaderView::Stretch);

  QHBoxLayout *barLayout = new QHBoxLayout();
  barLayout->addWidget(searchLabel);
  barLayout->addWidget(searchBar);
  barLayout->addWidget(searchButton);

  QVBoxLayout *mainLayout = new QVBoxLayout();
  mainLayout->addLayout(barLayout);
  mainLayout->addWidget(commitTable);

  this->setLayout(mainLayout);
}

void MyWindow::searchCommits()
{
  try
  {
    GITPP::REPO r("..");
    auto c = r.commits();

    std::string tString = searchBar->text().toStdString();

    int n = 0;
    int row = 0;
    bool found = false;

    for(auto i : c)
    {
      if (i.message().find(tString) != std::string::npos)
      {
        n++;
        commitTable->setRowCount(n);
        commitTable->setItem(row,0,new QTableWidgetItem(QString::fromStdString(i.message())));
        commitTable->setItem(row,1,new QTableWidgetItem(QString::fromStdString(i.author())));
        commitTable->setItem(row,2,new QTableWidgetItem(QString::fromStdString(i.time())));
        commitTable->setItem(row,3,new QTableWidgetItem(QString::fromStdString(i.id())));
        row++;

        found = true;
      }
    }

    if(!found)
    {
      QMessageBox msgBox;
      msgBox.setText("Could not find any commit messages containing that string.");
      msgBox.exec();
    }

  }
  catch(GITPP::EXCEPTION_CANT_FIND const&)
  {
    GITPP::REPO r(GITPP::REPO::_create);
  }
}

void MyWindow::makeConnection()
{
  connect(searchButton, SIGNAL(clicked()), this, SLOT(searchCommits()));
  connect(searchBar, SIGNAL(returnPressed()), this, SLOT(searchCommits()));
}

INSTALL_TAB(MyWindow, "Search");
