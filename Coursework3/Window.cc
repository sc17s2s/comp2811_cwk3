#include "Window.h"

Window::Window() : QWidget()
{
  setMinimumSize(640, 400);

  tabWidget = new QTabWidget();
  searchTab = new MyWindow();
  commitsTab = new HelloWorldLabel();
  selectTab = new SelectTab();
  configTab = new configureTab();
  branchesTab = new BranchesLabel();

  QHBoxLayout *layout = new QHBoxLayout();

  tabWidget->addTab(selectTab, "Select");
  tabWidget->addTab(branchesTab, "Branches");
  tabWidget->addTab(commitsTab, "Commits");
  tabWidget->addTab(configTab, "Configure");
  tabWidget->addTab(searchTab, "Search");

  layout->addWidget(tabWidget);

  this->setLayout(layout);
}
