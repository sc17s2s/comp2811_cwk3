#include "gitpp.h"
#include "globals.h"
#include <QtWidgets>

GITPP::REPO r;
static void functionS(){
	try{
		r.checkout("master");
		QMessageBox* check = new QMessageBox;
		check->setText("Switched to master branch");
		check->exec();
	} catch(GITPP::EXCEPTION){
		QMessageBox* error = new QMessageBox;
		error->setText("Error switching branch");
		error->exec();
	}

	// r.checkout(combo->currentText().toUtf8().constData());
}

namespace{

class BranchesLabel : public QWidget{
	//Q_OBJECT
public:
	BranchesLabel() : QWidget(){

		QVBoxLayout* mainLayout = new QVBoxLayout;

		QLabel* title = new QLabel("Branches");
		mainLayout->addWidget(title);

		QHBoxLayout* selectLayout = new QHBoxLayout;
		selectLayout->addWidget(new QLabel("Please select a branch:"));

		QComboBox* combo = new QComboBox();
		combo->addItem("Select branch");
		for(GITPP::BRANCH i : r.branches()){
			combo->addItem(QString::fromStdString(i.name()));
		}

		selectLayout->addWidget(combo);
		QPushButton* button = new QPushButton("Apply");
		selectLayout->addWidget(button);
		QObject::connect(button,&QPushButton::clicked,functionS);
		mainLayout->addLayout(selectLayout);
		mainLayout->addWidget(new QLabel(""));

		setLayout(mainLayout);

	}
};

INSTALL_TAB(BranchesLabel, __FILE__);

}
