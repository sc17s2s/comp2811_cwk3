#include <QtWidgets>
#include "gitpp.h"

class SelectTab : public QWidget
{
Q_OBJECT

public:
  SelectTab();
  void arrangeWidgets();

public slots:
  void selectRepo();
  void createRepo();
public:
  void makeConnection();

private:
  QLabel *selectLabel = new QLabel("Select directory to make a repo");
  QLabel *repoLabel = new QLabel("Select a repo");
  QPushButton *selectButton = new QPushButton("Create repo");
  QComboBox *selectDir = new QComboBox();
  QString selected;
};
