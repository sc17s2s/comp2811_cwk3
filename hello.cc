#include "globals.h"

#include <QLabel>

namespace{

class BranchesLabel : public QLabel{
public:
	BranchesLabel() : QLabel(){
		setText("Branches");
		setMinimumSize(640,400);
		//addStretch();
	}
};

INSTALL_TAB(BranchesLabel, "hello");

}
